# coding: utf8
import pdfkit
from flask import Flask, Response
from flask.globals import request


app = Flask('pdf_from_html')


@app.route('/pdf', methods=['POST'])
def generate_pdf_from_body():
    html = request.data
    options = {
        'page-size': 'Letter',
        'margin-top': '0.75in',
        'margin-right': '0.75in',
        'margin-bottom': '0.75in',
        'margin-left': '0.75in',
        'encoding': "UTF-8",
    }
    pdf = pdfkit.from_string(html, False, options=options)
    return Response(pdf, mimetype='text/pdf')


if __name__ == '__main__':
    app.run()
